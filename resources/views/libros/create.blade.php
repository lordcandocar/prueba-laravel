<!-- create.blade.php -->

@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            Añadir Libro
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <br />
            @endif
            <form method="post" action="{{ route('libros.store') }}">
                @csrf
                <div class="form-group">
                    <label for="titulo">Titulo Libro:</label>
                    <input type="text" class="form-control" name="titulo" value="{{ old('titulo') }}" />
                </div>
                <div class="form-group">
                    <label for="autor">Autor:</label>
                    <input type="text" class="form-control" name="autor" />
                </div>
                <div class="form-group">
                    <label for="num_paginas">Número de paginas:</label>
                    <input type="text" class="form-control" name="num_paginas" />
                </div>
                <div class="form-group">
                    <label for="precio">Precio :</label>
                    <input type="text" class="form-control" name="precio" />
                </div>
                <button type="submit" class="btn btn-primary">Añadir libro</button>
            </form>
        </div>
    </div>
@endsection
