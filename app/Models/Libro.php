<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    use HasFactory;
    protected $table = "libros";
    protected $primary_key = 'id';
    public $timestamps = true;

    protected $fillable = [
        'titulo',
        'autor',
        'num_paginas',
        'precio'
    ];
}
