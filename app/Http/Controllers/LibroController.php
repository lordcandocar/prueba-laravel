<?php

namespace App\Http\Controllers;

use App\Models\Libro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $libros = Libro::all();
        return view('libros.index', compact('libros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('libros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'titulo' => 'required',
            'autor' => 'required',
            'num_paginas' => 'required',
            'precio' => 'required|numeric'
        ], [
            'required' => 'El campo :attribute es requerido',
            'num_paginas.required' => 'El número de paginas es requerido.',
            'email' => 'El campo :attribute debe ser un formato email',
            'numeric'=> 'El campo :attribute debe ser númerico'
        ]);
        $libro = Libro::create($validate);
        return redirect('/libros')->with('success', 'Libro guardado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $libro = Libro::findOrFail($id);

        return view('libros.edit', compact('libro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'titulo' => 'required',
            'autor' => 'required',
            'num_paginas' => 'required',
            'precio' => 'required|numeric'
        ]);
        Libro::whereId($id)->update($validatedData);

        return redirect('/libros')->with('success', 'Libro actualizado éxitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $libro = Libro::findOrFail($id);
        $libro->delete();

        return redirect('/libros')->with('success', 'Libro eliminado éxitosamente.');
    }
}
